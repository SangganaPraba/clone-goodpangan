import React from 'react';
import Container from '@material-ui/core/Container';
import Appbar from "../components/Appbar";

export default function Homepage(){
    return (
        <React.Fragment>
            <Container maxWidth="sm">
                <Appbar/>
            </Container>
        </React.Fragment>
    )
}