import React from 'react';

export default function Appbar(){
    return (
        <div class="MuiContainer-root jss587 MuiContainer-maxWidthXs"
            style={{
                height: "100%",
                padding: 0,
                maxWidth: "444px",
                minHeight: "100vh",
                borderLeft: "1px solid #f1f1f1",
                borderRight: "1px solid #f1f1f1",
                paddingBottom: "56px",
                backgroundColor: "#FAFAFA",
                width: "100%",
                display: "block",
                boxSizing: "border-box",
                marginLeft: "auto",
                marginRight: "auto"
            }}
        >
        </div>
    )
}